(defproject hps-remind "1.0.0-SNAPSHOT"
  :description "The Heuristic Problem Solver with applications"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.csv "0.1.4"]
                 [org.clojure/tools.logging "0.4.0"]
                 [org.slf4j/slf4j-log4j12 "1.7.25"]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jmdk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.clojure/tools.cli "0.3.5"]
                 [amalloy/ring-buffer "1.2.1"]
                 [incanter/incanter-core "1.9.1"]
                 [cfft/cfft "0.1.0"]
                 [nz.ac.waikato.cms.weka/weka-stable "3.8.1"]
                 [hps-graphview "0.1.0-SNAPSHOT"]
                 [heuristic-problem-solver "1.0.0-SNAPSHOT"]]
  :aot [remind.core]
  :main remind.core)
