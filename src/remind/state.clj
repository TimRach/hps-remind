;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.state
  (:require [remind.entities :as ents]))


(def global-clock (remind.entities.Clock. (atom [])))

;;ACCELERATION
;;CSV Input
(def acceleration (atom nil))
(def accelerationModel (atom nil))
(def modelconfusion (atom nil))

;;Index of the current time step
(def rowindex (atom -1))

;;Accelerometer data
;;Complete list of log entries
(def accelData (atom nil))
;;values in current timestep
(def axisaccel (atom (hash-map)))

;;BLE Data
;;BLE Input
(def bluetoothLE (atom nil))
;;Complete list of log entries
(def bleData (atom nil))
;;values in current timestep
(def beacons (atom (hash-map)))

;;Alcohol Sensor Input
(def alcInput (atom nil))
;;Complete list of log entries
(def alcData (atom nil))
;;values in current timestep
(def alcsensorvalue (atom nil))

;;Step counter data
;;CSV Input
(def stepCSV (atom nil))
;;Complete list of log entries
(def stepData (atom nil))

(defn reset-data
  ([] reset-data bleData accelData alcData stepData)
  ([bledata acceldata alcoholdata stepdata]
   (do
     (reset! rowindex -1)
     (reset! bleData bledata)
     (reset! accelData acceldata)
     (reset! alcData alcoholdata)
     (reset! stepData stepdata )
     (reset! alcsensorvalue nil)
     (reset! axisaccel (hash-map))
     (reset! beacons (hash-map)))))

(defn set-line-index [idx]
  (do
    (reset! rowindex idx)
    (reset! alcsensorvalue (read-string (first (nth @alcData idx))))
    (reset! axisaccel (get accelData idx))
    (reset! beacons (get bleData idx))))

(defn next-data-line []
  (when (< (inc @rowindex) (count @stepData))
    (swap! rowindex inc)
    (set-line-index @rowindex)))

(defn prev-data-line []
  (when (>= (dec @rowindex) 0)
    (swap! rowindex dec)
    (set-line-index @rowindex)))
