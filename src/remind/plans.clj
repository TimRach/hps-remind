(ns remind.plans
  (:require
   [hps.utilities.math :as mut]))


(def clock (atom 0))
(def eventhistory (atom '()))
(def apronstate (atom false))

(defrecord Rule [event condition consequence alternative])
(defrecord Trigger [consequnce &triggerfunctions])


(def eventtypes [:None :enterzone :stayinzone :putonapron :dropapron :exitzone :HD])




(defn p= [tocheck expected]
  (if (or (nil? tocheck) (nil? expected))
    0
    (if (= (:value tocheck) expected)
      (:aggregated-evaluation tocheck)
      (- 1 (:aggregated-evaluation tocheck)))))

(defn p!= [tocheck expected]
  (if (or (nil? tocheck) (nil? expected))
    0
    (- 1 (p= tocheck expected))))

(defn p=p [dec1 dec2]
  (if (or (nil? dec1) (nil? dec2))
    0
    (if (= (:value dec1) (:value dec2))
      (mut/average [(:aggregated-evaluation dec1) (:aggregated-evaluation dec2)])
      (- 1 (mut/average [(:aggregated-evaluation dec1) (:aggregated-evaluation dec2)])))))

(defn p!=p [dec1 dec2]
  (if (or (nil? dec1) (nil? dec2))
    0
    (- 1 (p=p dec1 dec2))))

;;Aktion Enter-Patientzone
;;Condition: Zone != Old_Zone
;;Condition: Zone != None
;;Condition: Movement != Standing
(def enterzone {:name :enterzone
                :conditions {:zone (fn [state]
                                     (let [oldnotnew (p!=p (:oldzone state)
                                                           (:currzone state))
                                           newnotnone (p!= (:currzone state) :None)]
                                       (mut/average [(p!=p (:oldzone state) (:currzone state))
                                                     (p!= (:currzone state) :None)])))
                             :movement (fn [state]
                                         (let [movnotstand (p!= (:currmov state) :stand)]
                                           movnotstand))}})
;;Aktion exitzone
;;Condition: Old_Zone != None
;;Condition: Zone != Old_Zone
;;Condition: Movement != Standing
(def exitzone {:name :exitzone
               :conditions {:zone (fn [state]
                                    (p!=p (:oldzone state)  (:currzone state)))
                            :movement (fn [state] (p!=  (:currmov state)  :stand))}})
;;Aktion Stay-in-Patientzone
;;Condition: Zone = Old_Zone
;;Condition: Zone != :None
(def stayinzone {:name :stayinzone
                 :conditions {:zone (fn [state] (mut/average [(p=p (:oldzone state) (:currzone state))
                                                             (p!= (:currzone state) :None)]))}})
(defn make-event [type time]
  {:event type :time time})

(defn remove-watches
  [identifier & references]
  (for [ref references]
    (remove-watch ref identifier)))


(def timedapron (Rule. :enterzone
                       (fn [time] true)
                       (fn [time]
                         (let [identifier (str "enterzone-apron" time)]
                           (add-watch clock identifier
                                      (fn [k a o n] (when (and (> n (+ time 120))
                                                               (= @apronstate false))
                                                      (println "No Apron after 120 seconds in zone")
                                                      (remove-watches identifier clock))))))
                       (fn [time] nil)))


(def beforecontact (Rule. :enterzone
                          (fn [time] (not= (:event (second @eventhistory)) :HD))
                          (fn [time] (println "Missing HD before entering a zone"))
                          (fn [time] (println "Saw previous HD - Rule OK"))))

(def aftercontact (Rule. :exitzone
                         (fn [time] true)
                         (fn [time] (let [identifier (str "exitzone-timed" time)]
                                     (add-watch clock identifier
                                                (fn [k a o n] (when (> n (+ time 20))
                                                               (println "Missing HD 20 seconds after leaving a zone")
                                                               (remove-watches identifier clock eventhistory))))
                                     (add-watch eventhistory identifier
                                                (fn [k a o n] (if (= (:event (first n)) :HD)
                                                               (do
                                                                 (println "Registered HD - Rule OK")
                                                                 (remove-watches identifier clock eventhistory))
                                                               (do
                                                                 (println "Missing HD after leaving a zone")
                                                                 (remove-watches identifier clock eventhistory)))))))
                         (fn [time] nil)))

(defn check-rule [event rule]
  (when (= (:event event) (:event rule))
    (if ((:condition rule) @clock)
      ((:consequence rule) @clock)
      ((:alternative rule) @clock))))

(defn check-rules [rules]
  (dorun (map #(check-rule (first @eventhistory) %) rules)))


(defn add-event [eventtype]
  (let [evt (make-event eventtype @clock)]
    (swap! eventhistory conj evt)
    evt))


(defn eval-action [action state]
  (let [conds (:conditions action)]
    (mut/round2 3 (mut/average (map #(% state) (vals conds))))))

(def actions [exitzone enterzone stayinzone])
(def rulestack [beforecontact aftercontact timedapron])
