;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.GUI.controls
  (:require [hps.GUI.make-decision :refer :all]
            [hps.decision-modules :as dms]
            [hps.utilities.gui :as gut]
            [remind.state :as state]
            [remind.experts :as experts])
  (:import (javafx.scene.control Slider Button)
           (javafx.scene.layout HBox VBox)
           (javafx.beans.value ChangeListener)))

(defmacro add-listener [property action]
  `(.addListener ~property
                 (reify ChangeListener
                   (changed [this obsval oldval newval]
                     ~action))))


(defn make-control-buttons [scene]
  (let [run-button-box (HBox.)
        button-run (Button. "Start DMs")
        button-run-all (Button. "Evaluate [0 -> t]")
        button-next (Button. ">")
        button-prev (Button. "<")]
    (gut/add-to-container run-button-box button-prev button-run button-run-all button-next)
    (.add (.getStyleClass run-button-box) "control-buttons-hbox")
    (gut/set-button-action button-prev (state/prev-data-line))
    (gut/set-button-action button-next (state/next-data-line))
    (gut/set-button-action button-run (experts/start-everything))
    (gut/set-button-action button-run-all
                           (doseq [dec (experts/run-dms-stepwise)]
                             (let [dm-pane (.lookup scene (str "#dm-pane-tab-" (name (key dec))))]
                               (update-decision-display @(:internal-decision (dms/get-dm-from-name (key dec) 'remind.experts)) dm-pane))))
    run-button-box))


(defn make-control-slider []
  (let [slider (Slider.)]
    (.setMin slider 0)
    (.setMax slider 100)
    (.setMajorTickUnit slider 1)
    (.setSnapToTicks slider true)
    (.setValue slider 0)
    (add-watch state/accelData :slider (fn [k a o n]
                                         (.setMax slider (dec (count n)))))
    (add-watch state/rowindex :slider (fn [k a o n]
                                        (.setValue slider n)))
    (add-listener (.valueProperty slider)
                  (state/set-line-index (int (.getValue slider))))
    slider))

(defn make-control-elements [scene]
  (let [ctrl-wrap (VBox.)
        buttons (make-control-buttons scene)
        slider (make-control-slider)]
    (gut/add-to-container ctrl-wrap slider buttons)
    ctrl-wrap))

