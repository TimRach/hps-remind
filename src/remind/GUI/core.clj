;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.GUI.core
  (:require [clojure.java.io :as io]
            [hps.GUI.structure :as structure]
            remind.GUI.controls
            remind.GUI.display
            remind.GUI.menu
            [remind.state :as state]
            [hps.decision-modules :as dm]
            [hps.utilities.gui :as gut]
            [remind.experts :as experts])
  (:import [javafx.event Event EventHandler]
           [javafx.scene.control Tab TabPane]
           [javafx.scene.layout BorderPane HBox Priority]))

(def dm-colors {:Graphview ["ADF3FF" "ADF3FF"]
                :Actions ["5D8AAD" "5D8AAD"]
                :Selector ["EA4C26" "EA4C26"]
                :HDRecognition ["feffb3" "feffb3"]
                :State ["00caef" "00caef"]
                :Movement ["ffb800" "ffb800"]
                :Zone  ["A9BA54" "A9BA54"]})

(defn load-dm-configuration [dm-name]
  (get (dm/list-decision-modules) (keyword dm-name)))


(defn initialize-expert-display [hps-displays dm-keyword]
  (let [experts-pane (some #(when (= (.getId %) "hps-configbox") %) (.getItems hps-displays))
        {:keys [producers evaluators adaptors] :as experts} (:available-experts (dm/get-dm-from-name dm-keyword 'remind.experts))
        [producer-pane adaptor-pane evaluator-pane parameters-pane :as panes] (vec (.getChildren experts-pane))]
    (run! (fn [[pane experts]]
            (hps.GUI.make-decision/initialize-experts-in-display pane experts)
            (.setSelected (some #(when (= (.getText %) "All") %) (.getChildren (.getContent pane))) true))
          [[producer-pane producers] [evaluator-pane evaluators] [adaptor-pane adaptors]])))


(defn make-decision-module-tabs [tab-pane decision-modules]
  (doseq [dm-keyword decision-modules]
    (let [tab (Tab. (name dm-keyword))
          tab-content-pane (BorderPane.)
          tabcontentpane-hps-displays (structure/make-decision-module-basic-display)
          [fg-color bg-color] (get dm-colors dm-keyword)
          display-parameters (dissoc hps.make-decision/produce-and-evaluate-default-params :evaluator-aggregation-fun)]
      (initialize-expert-display tabcontentpane-hps-displays dm-keyword)
      (.setId tabcontentpane-hps-displays (str "dm-pane-tab-" (name dm-keyword)))
      (.setDividerPosition tabcontentpane-hps-displays 0 0.4)
      (.add (.getStyleClass tab-content-pane) "morph-tabpane-with-goal")
      (.setStyle tab (str "-fx-background-color: #" bg-color ";"))
      (.setStyle tab-content-pane (str "-fx-background-color: #" bg-color ";"))
      (.setCenter tab-content-pane tabcontentpane-hps-displays)
      (.setContent tab tab-content-pane)
      (.add (.getTabs tab-pane) tab)
      (.setOnSelectionChanged tab
                              (proxy [EventHandler] []
                                (handle [^Event event]
                                  (when (.isSelected tab)
                                    (load-dm-configuration(.getText tab)))))))))

(defn make-graphview-tab [decision-modules scene]
  (let [tab (Tab. "Graphview")
        container (BackgroundCanvas. 100 100)]
    (doseq [inputsrc [state/bluetoothLE state/acceleration state/stepCSV state/alcInput]]
      (add-watch inputsrc
                 :graphview
                 (fn [k a o n]
                   (let [fields {:filename (.getName (:filename n)) :fields (:fields n)
                                 :frequency (:frequency n) :buffersize (:buffersize n)}]
                     (.addGenericNode container (str "CSV File :" (:filename fields)) fields)
                     (remove-watch inputsrc :graphview)))))
    (doseq [dmname decision-modules]
      (let [dm (dm/get-dm-from-name dmname 'remind.experts)]
        (.addDecisionModule container dm)
        ;;Add callback to java source for data changes
        (add-watch (:internal-decision dm) :graphview
                   (fn [k a o n] (.updateGraphData container (name (:name dm)) n)))))
    (when-not (nil? (dm/list-dm-connections 'remind.experts))
      (let [conns (dm/list-dm-connections 'remind.experts)
            from (keys conns)
            to (map #(first (get conns %)) (keys conns))]
        (.connectDecisionModules container
                                 (java.util.ArrayList. from)
                                 (java.util.ArrayList. to))))
    (.setScene container scene)
    (.setContent tab container)
    tab))

(defn display-decision-process [scene]
  (let [hps-split-pane (.lookup scene "#hps-centerSplitPane")
        controlRegion  (.lookup scene "#hps-controlRegion")
        [decisionModules stateRegion] (vec (.getItems hps-split-pane))
        decision-modules-tab-pane (TabPane.)
        graphtab (make-graphview-tab (dm/list-decision-modules 'remind.experts) scene)
        info-and-control-box (remind.GUI.controls/make-control-elements scene)]

    (.setId decision-modules-tab-pane "dm-tabpane")
    (gut/add-to-container controlRegion info-and-control-box)
    (gut/add-to-container decisionModules decision-modules-tab-pane)

    (HBox/setHgrow decision-modules-tab-pane Priority/ALWAYS)
    (.setDividerPosition hps-split-pane 0 0.6)

    (make-decision-module-tabs decision-modules-tab-pane
                               (dm/list-decision-modules 'remind.experts))
    (.add (.getTabs decision-modules-tab-pane) graphtab)
    (.setId (.getContent graphtab) "graph-view-container")
    (gut/add-to-container stateRegion (remind.GUI.display/make-display))))


(defn adjust-gui-for-remind [scene]
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/remind.css")))
  (.add (.getStylesheets scene) (.toExternalForm (io/resource "css/graphview.css")))
  (remind.GUI.menu/adjust-menu-for-remind (.lookup scene "#hps-menubar-left") scene)
  (display-decision-process scene))


;;always add "REM" mode
(hps.GUI.structure/register-hps-mode "REM" adjust-gui-for-remind true)
