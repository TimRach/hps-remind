;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.GUI.charts
  (:require [remind.state :as state])
  (:use [remind.GUI.chartsutil]))



(defn hd-chart []
  (make-line-chart [{:name :AlcSensor :hook state/rowindex :datasrc state/alcData :index 0}]))

(defn steps-chart []
  (make-line-chart [{:name :Steps :hook state/rowindex :datasrc state/stepData :index 0}]))

(defn ble-chart []
  (make-line-chart [{:name :Zone1 :hook state/rowindex :datasrc state/bleData :index 0}
                    {:name :Zone2 :hook state/rowindex :datasrc state/bleData :index 1}
                    {:name :Zone3 :hook state/rowindex :datasrc state/bleData :index 2}]))

(defn accel-chart []
  (make-line-chart [{:name :X :hook state/rowindex :datasrc state/accelData :index 0}
                    {:name :Y :hook state/rowindex :datasrc state/accelData :index 1}
                    {:name :Z :hook state/rowindex :datasrc state/accelData :index 2}]))

;; (defn ble-chart []
;;   (make-basic-line-chart state/bluetoothLE))
;;
