;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.GUI.display
  (:require [remind.GUI.charts :as charts]
            [remind.state :as state]
            [hps.utilities.gui :as gut])
  (:import [javafx.scene.control CheckBox Label Tab TabPane]
           [javafx.scene.layout BorderPane GridPane VBox]))

(defn make-status-fields []
  (let [wrapper (GridPane.)
        risk-checker (CheckBox.)
        risk-label (Label. "HD pls")
        activity-label (Label. "Detected activity")
        activity-text (Label. " - None -")
        line-label (Label. "Entry") line-value (Label. "-1")
        time-label (Label. "Timestamp")  time-value (Label. "HH:MM:SS")
        sensor-label (Label. "Sensor") sensor-value (Label. "-1")
        step-label (Label. "Step") step-value (Label. "-1")
        alcohol-conc-label(Label. "Alcohol-Conc") alcohol-conc-value (Label. "-1")
        last-hd-label (Label. "Last-Hd") last-hd-value (Label. "-1")]

    (.setDisable risk-checker true)
    (.setSelected risk-checker true)
    (add-watch state/rowindex :gui (fn [k a o n] (.setText line-value (str n))))
    (.add wrapper line-label 0 0) (.add wrapper line-value 1 0)
    (.add wrapper time-label 0 1) (.add wrapper time-value 1 1)
    (.add wrapper sensor-label 0 2) (.add wrapper sensor-value 1 2)
    (.add wrapper step-label 0 3) (.add wrapper step-value 1 3)
    (.add wrapper alcohol-conc-label 0 4) (.add wrapper alcohol-conc-value 1 4)
    (.add wrapper last-hd-label 0 5) (.add wrapper last-hd-value 1 5)
    (.setHgap wrapper 50)
    wrapper))


(defn make-tab [name element]
  (let [tab (Tab. name)
        tab-content (BorderPane.)]
    (.setCenter tab-content element)
    (.setContent tab tab-content)
    tab))


(defn make-display []
  (let [tab-pane (TabPane.)
        hd-wrapper (VBox.)]
    (gut/add-to-container hd-wrapper (charts/hd-chart) (make-status-fields))
    (.add (.getTabs tab-pane) (make-tab "HD-Sensor" hd-wrapper))
    (.add (.getTabs tab-pane) (make-tab "BLE Sensor" (charts/ble-chart)))
    (.add (.getTabs tab-pane) (make-tab "Accelerometer" (charts/accel-chart)))
    (.add (.getTabs tab-pane) (make-tab "Steps" (charts/steps-chart)))
    tab-pane))

