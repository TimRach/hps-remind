;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.GUI.chartsutil
  (:require [hps.utilities.gui :as gut])
  (:import [javafx.scene.chart LineChart NumberAxis]
           javafx.scene.layout.StackPane))


(defn convert-entry [entry]
  (when-not (or (nil? entry) (= "" entry))
    (read-string entry)))


(defn remove-series-data [pos series]
  (let [entries (.size (.getData series))]
    (.remove (.getData series) pos entries)))


(defn add-series-data [pos series datasource dataindex]
  (let [entries (.size (.getData series))
        diff (- (inc pos) entries)
        toinsert (map #(convert-entry (get % dataindex)) (take diff (drop entries datasource)))
        xvals (range entries (inc pos))]
    (.addAll (.getData series) (map #(javafx.scene.chart.XYChart$Data. (first %1) (second %1))
                                    (filter #(not (nil? (second %1))) (map vector xvals toinsert))))))

(defn on-data-change
  ([k a o n series data] (on-data-change k a o n series data 1 identity))
  ([k a o n series data idx] (on-data-change k a o n series data idx identity))
  ([k a o n series data idx add-preprocess]
   (if (and (>= n o) (not (nil? data)))
     (add-series-data (add-preprocess n) series data idx))
   (if (and (< n o) (not (nil? data)))
     (remove-series-data n series))))

(defn make-series [args]
  (for [arg args]
    (let [series (javafx.scene.chart.XYChart$Series.)]
      (add-watch (:hook arg) (:name arg)
                 (fn [k a o n] (on-data-change k a o n series @(:datasrc arg) (:index arg))))
      (.setName series (name (:name arg)))
      series)))

(defn make-line-chart [args]
  (let [wrapper (StackPane.)
        xaxis (NumberAxis.)
        yaxis (NumberAxis.)
        chart (LineChart. xaxis yaxis)]
    (gut/add-to-container wrapper chart)
    (.addAll (.getData chart) (make-series args))
    (.setAnimated chart false)
    (.setCreateSymbols chart false)
    wrapper))


(defn series-from-datasource [chart datasrc]
  (for [field (:fields datasrc)]
    (let [series (javafx.scene.chart.XYChart$Series.)]
      (add-watch (:buffer datasrc) (str :chart field)
                 (fn [k a o n] (println field " changed " n "(" (str :chart field) ")")))
      (.setName series (name field))
      series)))


(defn make-basic-line-chart [datasrc]
  (let [wrapper (StackPane.)
        xaxis (NumberAxis.)
        yaxis (NumberAxis.)
        chart (LineChart. xaxis yaxis)]
    (gut/add-to-container wrapper chart)
    (add-watch datasrc :chart
               (fn [k a o n]
                 (.addAll (.getData chart) (series-from-datasource chart n))
                 (.setAnimated chart false)
                 (.setCreateSymbols chart false)))
    wrapper))
