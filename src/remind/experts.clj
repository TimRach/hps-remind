;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.experts
  (:require
   [clojure.math.combinatorics :as comb]
   [hps.decision-modules :refer :all]
   [hps.heuristics :refer :all]
   [hps.make-decision :as md]
   [hps.types :refer :all]
   [remind.config :as cfg]
   [remind.plans :as plans]
   [remind.state :as state :refer :all]
   [remind.memory :as mem]
   [remind.entities :refer [reset nexttick ontick start]]
   [incanter.stats :as stats]
   [cfft.core] [cfft.matrix] [cfft.complex]
   [hps.utilities.math :as mut]
   [hps.utilities.misc :as utmisc])
  (:import [weka.core Instances DenseInstance Attribute FastVector]
           [hps.types DecisionAlternative]))


;; =======================================
;; ======== ZONE ESTIMATION ==============
;; =======================================


(defn get-window-data [buffer]
  (dissoc (apply (partial merge-with (comp vec flatten vector)) buffer) :Index))

(defn average-signal-strength [buffer]
  (mapv #(when (seq %1) (float (mut/average %1))) (map (partial remove nil?) (vals (get-window-data buffer)))))

(defn value-combinations [values]
  (map utmisc/without-nth (repeat (count values) values) (range (count values))))

;;Bewertung als linearer Wert [0,1] zwischen minimum und maximum.
;;Bewertung(Maximum) = 1, Bewertung(Minimum) = 0
(def-heuristic threshold-evaluator
  :evaluator (def-expert-fun [alts :optional [params {:window-size 1 :max-ble-value -50 :min-ble-value -100}]]
               (let [evals (map #(if (nil? %1) 0.0 (/ (- (:min-ble-value params) %1)
                                                      (- (:min-ble-value params) (:max-ble-value params))))
                                (average-signal-strength @(:buffer @state/bluetoothLE)))]
                 (zipmap alts evals))))


(defn deviationfn [alts buffer scale]
  (let [single-averages (average-signal-strength buffer)
        other-averages  (reverse (map #(mut/average (remove nil? %1))
                                      (value-combinations single-averages )))]
    (zipmap alts (map #(if-not (nil? %1) (/ (- %1 (or %2 %1)) 30) -1)
                      single-averages other-averages))))

;; Calculates the averages for all signals in the data window and the difference of
;; each average to the mean of the other averages. This difference is scaled with the
;; parameter max-deviation to produce ratings in [-1,1]
(def-heuristic deviation
  :evaluator (def-expert-fun [alts :optional [params {:window-size 1 :max-deviation 20}]]
               (if (pos? (count @(:buffer @state/bluetoothLE)))
                 (deviationfn alts @(:buffer @state/bluetoothLE) (:max-deviation params))
                 (utmisc/map-to-kv (fn [alts] 0.0) alts))))

(def-heuristic zones
  :producer (def-expert-fun [] (:fields @state/bluetoothLE)))


(declare-dm :Zone
            :available-experts {:producers [:zones]
                                :evaluators [:threshold-evaluator :deviation]}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers [:zones]
                                         :evaluators
                                         [[:deviation :weight 1.0 :window-size cfg/ble-window-size :max-deviation 30]
                                          [:threshold-evaluator :weight 1.0
                                           :max-ble-value cfg/ble-max :min-ble-value cfg/ble-min]]}}))
            :onacceptance-fun (fn [dec] (if (> (:aggregated-evaluation dec) cfg/zone-accept)
                                         (mem/add-zone dec)
                                         (mem/add-zone (DecisionAlternative. :None [] [] []
                                                                             (- 1 (:aggregated-evaluation dec))))))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :raw :equal-weights]
                                  :run-frequency-fun (run-frequency-fun-by-time (* cfg/ble-decision-interval
                                                                                   cfg/single-tick-time))
                                  :clocked-interval cfg/ble-decision-interval})

;; =======================================
;; =========== ACCELERATION ==============
;; =======================================
;;Possible classification values

(defn preprocessData
  "Adds the acceleration to each [x y z] row"
  [data]
  (mapv #(conj % (mut/vector-length %)) data))


(defn makeFeatures [window]
  (let [transposed (apply map list (preprocessData window)) ;Transforms [[x,y],[x,y]..] to [[x,x,..],[y,y,..]]
        means (map mut/average transposed)
        vars (map stats/variance transposed)
        corrs (map #(stats/correlation (first %) (second %)) (comb/combinations transposed 2))
        fftnrg (map #(/ (mut/vector-length (cfft.matrix/matrix-apply cfft.complex/real (cfft.core/fft %)))
                        (count %)) transposed)]
    (concat means vars corrs fftnrg)))

(defn makeInstance [featvec dataset]
  (let [inst (DenseInstance. (inc (count featvec)))
        attfeat (map list (range (inc (count featvec))) featvec)]
    (doseq [pair attfeat]
      (.setValue inst (first pair) (second pair)))
    (.setDataset inst dataset)
    inst))

(def initialset (let [numattrs (mapv #(Attribute. %) (seq cfg/numericAttributes))
                      classattr (Attribute. "CLASS" (seq cfg/classValues))
                      attributes (conj numattrs classattr)
                      attinfo (FastVector. (count attributes))]
                  (doseq [attr attributes]
                    (.addElement attinfo attr))
                  (let [instances (Instances. "Initial" attinfo 0)]
                    (.setClassIndex instances (dec (.numAttributes instances)))
                    instances)))

(defn classify []
  (let [features (makeFeatures (mapv vals @(:buffer @state/acceleration)))
        instance (makeInstance features initialset)
        eval (weka.classifiers.Evaluation. initialset)]
    (.useNoPriors eval)
    (keyword (get cfg/classValues
                  (int (.evaluateModelOnce eval @state/accelerationModel instance))))))

(def-heuristic acceleration-states
  :producer (def-expert-fun [] (map keyword cfg/classValues)))

(def-heuristic confusion-matrix
  :evaluator (def-expert-fun [alts]
               (when (>= (count @(:buffer @state/acceleration))
                         (:buffersize @state/acceleration))
                 (let [class (classify)
                       classindex (.indexOf cfg/classValues (name class))
                       totalpop (apply + (first @state/modelconfusion))
                       altvalues(map #(.indexOf cfg/classValues (name (:value %))) alts)
                       confusion (get @state/modelconfusion classindex)
                       ordered (map #(get confusion %) altvalues)]
                   (zipmap alts  (map #(/ % totalpop) ordered))))))

(declare-dm :Acceleration
            :available-experts {:producers [:acceleration-states]
                                :evaluators  [:confusion-matrix]}
            :configure-fun (fn [_] (map->DecisionSetup
                                   {:experts {:producers [:acceleration-states]
                                              :evaluators [:confusion-matrix]}}))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :normalized :equal-weights]
                                  :clocked-interval cfg/accel-decision-interval
                                  :run-frequency-fun (run-frequency-fun-by-time                                                                                 (* cfg/single-tick-time cfg/accel-decision-interval))})

;; =======================================
;; ========= MOVEMENT STATE ==============
;; =======================================

(def-heuristic movement-state
  :producer (def-expert-fun []
              (list :stand :walk))
  :evaluator (def-expert-fun [alts :optional [params {:walk-thresh 3}]]
               (utmisc/map-to-kv (fn [alt]
                                   (if (pos? (count @(:buffer @state/stepCSV)))
                                     (let [walk-certainty (/ (min (:walk-thresh params)
                                                                  (:steps (last @(:buffer @state/stepCSV))))
                                                             (:walk-thresh params))]
                                       (cond
                                         (= (:value alt) :walk) walk-certainty
                                         (= (:value alt) :stand) (- 1 walk-certainty)))
                                     0.0)) alts)))

(declare-dm :Movement
            :available-experts {:producers [:movement-state]
                                :evaluators [:movement-state]}
            :configure-fun (fn [_] (map->DecisionSetup {:experts
                                                       {:producers [:movement-state]
                                                        :evaluators [[:movement-state
                                                                      :weight 1.0
                                                                      :walk-thresh 4]]}}))
            :run-frequency-fun (run-frequency-fun-by-time (* cfg/step-decision-interval
                                                             cfg/single-tick-time))
            :onacceptance-fun(fn [dec] (swap! mem/move-ring conj dec))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :normalized :equal-weights]
                                  :clocked-interval cfg/step-decision-interval})


;; =======================================
;; ========= HD Recognition ==============
;; =======================================

(def-heuristic alc-gradient
  :evaluator (def-expert-fun [alts :optional [params {:window-size 25
                                                      :gradient-thresh 100}]]
               (utmisc/map-to-kv (fn [alt]
                                   (let [startx (max (- @state/rowindex (:window-size params)) 0)
                                         endx @state/rowindex
                                         starty (read-string (first (nth @state/alcData startx)))
                                         endy @state/alcsensorvalue]
                                     (max 0 (min 1
                                                 (float (/ (/ (- endy starty) (max (- endx startx) 1))
                                                           (:gradient-thresh params))))))) alts)))

(def-heuristic alc-threshold
  :evaluator (def-expert-fun [alts :optional [params {:alcohol-thresh 3000}]]
               (utmisc/map-to-kv (fn [alt]
                                   (min 1
                                        (/ @state/alcsensorvalue (:alcohol-thresh params))))
                                 alts)))

(def-heuristic hand-desinfection
  :producer (def-expert-fun [] :hand-desinfection))


(declare-dm :HDRecognition
            :available-experts {:producers [:hand-desinfection]
                                :evaluators [:alc-threshold  :alc-gradient]}
            :configure-fun (fn [_] (map->DecisionSetup {:experts
                                                       {:producers [:hand-desinfection]
                                                        :evaluators [[:alc-gradient
                                                                      :weight 1.0]
                                                                     [:alc-threshold
                                                                      :weight 1.0]]}}))

            :run-frequency-fun (run-frequency-fun-by-time (* cfg/alc-decision-interval
                                                             cfg/single-tick-time))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :normalized :equal-weights]
                                  :clocked-interval cfg/alc-decision-interval})


;; =======================================
;; ========== STATE ESTIMATOR ============
;; =======================================

(def-heuristic actions :producer (def-expert-fun []  (seq plans/eventtypes)))

;;---> Hier weitermachen: verlgeiche innerhalb des actions/plans moduls brauchen werte zwischen 1 und 0
;;     deshalb kommt hier quatch raus.
;;---> Gesamt Zustand übersicht in graphview wäre cool

(def-heuristic action-characteristics :evaluator
  (def-expert-fun [alts]
    (utmisc/map-to-kv
     (fn [alt]
       (let [plan (first (filter #(= (:name %) (:value alt)) plans/actions))]
         (if (nil? plan)
           0.0
           (plans/eval-action plan {:oldmov (first @mem/move-ring) :currmov (second @mem/move-ring)
                                    :oldzone @(:previous-zone mem/state)
                                    :currzone @(:current-zone mem/state)})))) alts)))

(declare-dm :Actions
            :available-experts {:producers [:actions]
                                :evaluators [:action-characteristics]}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers [:actions]
                                         :evaluators [:action-characteristics]}}))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :normalized :equal-weights]
                                  :run-frequency-fun (run-frequency-fun-by-time (* cfg/ble-decision-interval
                                                                                   cfg/single-tick-time))
                                  :clocked-interval cfg/ble-decision-interval})




(def-heuristic occurrence
  :evaluator (def-expert-fun [alts]
               (let [freqs (frequencies (map #(:value %) @mem/zone-ring))]
                 (utmisc/map-to-kv
                  (fn [alt] (get freqs (:value alt))) alts))))

(def-heuristic zones-buffer
  :producer (def-expert-fun []
              (let [dec @(:executed-decision (get-dm-from-name :Zone 'remind.experts))]
                (distinct (map #(:value %) @mem/zone-ring)))))


(declare-dm :Selector
            :available-experts {:producers [:zones-buffer]
                                :evaluators [:occurrence]}
            :configure-fun (fn [_]
                             (map->DecisionSetup
                              {:experts {:producers [:zones-buffer]
                                         :evaluators [:occurrence]}}))
            :onacceptance-fun (fn [dec] (mem/new-zone-decision dec))
            :decision-parameters {:evaluator-aggregation-fun [:weighted-sum :normalized :equal-weights]
                                  :run-frequency-fun (run-frequency-fun-by-time (* cfg/ble-decision-interval
                                                                                   cfg/single-tick-time))
                                  :clocked-interval cfg/ble-decision-interval})



(declare-dm-connections :decision-to-data [{:from :Zone :to :Selector}
                                           {:from :Selector :to :Actions}
                                           {:from :Zone :to :Actions}
                                           {:from :Movement :to :Actions}])


;; =======================================
;; ============= UTIL ====================
;; =======================================

(defn setup-experts []
  (println "Setup experts.."))

(defn tear-down []
  (println "Tear down expert setup..."))

(defn start-everything []
  (println "Start input streams ...")
  (start @state/bluetoothLE)
  (start @state/acceleration)
  (start @state/stepCSV)
  (doseq [dm (list-decision-modules 'remind.experts)]
    (start-dm dm 'remind.experts)
    (println "Start decision module " dm)))


(defn run-dms [n]
  (loop [names (list-decision-modules 'remind.experts)
         result {}]
    (if (nil? (first names))
      result
      (if (and (pos? n)
               (zero? (rem n (:clocked-interval (:decision-parameters
                                                 (get-dm-from-name (first names) 'remind.experts))))))
        (let [dec (step-decision-module (get-dm-from-name (first names) 'remind.experts))]
          (recur (rest names)
                 (assoc result (keyword (first names)) dec)))
        (recur (rest names) result)))))



(defn checkZoneStates [n]
  (let [action (:value @(:executed-decision (get-dm-from-name :Actions 'remind.experts)))]
    (cond
      (= action :enterzone) (when (and (= @(:entered-zone mem/state) :None)
                                       (not= (:value @(:current-zone mem/state)) :None))
                              (println n "Entered Zone" (:value @(:current-zone mem/state)))
                              (reset! (:entered-zone mem/state) @(:current-zone mem/state)))
      (= action :exitzone) (when (not= @(:entered-zone mem/state) :None)
                             (println n "Exit Zone" (:value @(:entered-zone mem/state)))
                             (reset! (:entered-zone mem/state) :None)))))


;;Reset everything
(defn reset-decision-scope []
  (reset @state/bluetoothLE)
  (reset @state/acceleration)
  (reset @state/stepCSV)
  (reset @state/alcInput)
  (mem/reset-state))

(defn run-dms-stepwise []
  (let [goal @state/rowindex]
    (reset-decision-scope)
    (loop [n 0
           decisions []]
      (if (< n goal)
        (do
          ;; (println (:value @(:executed-decision (get-dm-from-name :Actions 'remind.experts))))
          ;; (println (:value @(:executed-decision (get-dm-from-name :Selector 'remind.experts)))
          ;;          " - " (:value @(:previous-zone mem/state)) " -> " (:value @(:current-zone mem/state)) " - " (:value @(:executed-decision (get-dm-from-name :Actions 'remind.experts))))
          (checkZoneStates n)
          ;; (ontick state/global-clock n)
          (nexttick @state/bluetoothLE)
          (nexttick @state/acceleration)
          (nexttick @state/stepCSV)

          ;; (if (= (:value @(:current-zone mem/state)) :None)
          ;;   (println 0)
          ;;   (println 1))
          ;; (println (map #(str (:value %) " - ") (vals decisions)))
          (recur (inc n)
                 (run-dms n)))
        (do
          decisions)))))
