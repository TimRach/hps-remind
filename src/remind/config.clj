;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.config)

;; =======================================
;; ============= BLE ZONES ===============
;; =======================================
(def ble-window-size 10);How many seconds of datapoints are stored in the window
(def ble-min -100)
(def ble-max -60)
(def zone-accept 0.75)

(def dictator-buffer-size 10)

;; =======================================
;; =========== ACCELERATION ==============
;; =======================================

(def classValues ["TREADMILL_WALKING_FLAT""TREADMILL_WALKING_INCL""TREADMILL_RUNNING""STEPPER""CROSS_TRAINER""CYCLING_HORIZONTAL""CYCLING_VERTICAL""ROWING""JUMPING""BASKETBALL""SITTING""STANDING""LYING_BACK""LYING_RIGHT""STAIRS_UP""STAIRS_DOWN""ELEVATOR_STANDING""ELEVATOR_MOVING""PARKING_MOVING"])

(def numericAttributes ["MEAN_X" "MEAN_Y"	"MEAN_Z"	"MEAN_A"	"VAR_X"	"VAR_Y"	"VAR_Z"	"VAR_A"	"CORRXY"	"CORRXZ"	"CORRXA"	"CORRYZ"	"CORRYA"	"CORRZA"	"ENERGY_X"	"ENERGY_Y"	"ENERGY_Z"	"ENERGY_A"])

;; window size in SECONDS!
(def accel-window-size 5)
;; overlap in SECONDS!
(def accel-window-overlap 2.5)



;; =======================================
;; ============== TIMING =================
;; Following times are hardcoded according to the respective recording/sampling frequency.
;; BLE for example is sampled at 10Hz and the BLE-decision module will give a decision at
;; the same frequency. At a single tick interval of 50Hz this results in a ble-input-interval
;; and a ble-decision-interval of 5 ticks.
;; No value should be less than single-tick-freq, and at best be multiples of it

(def single-tick-freq 20) ; -> 50Hz
(def single-tick-time (/ 1000 single-tick-freq))
(def ble-sample-freq 10)  ; -> 10Hz
(def accel-sample-freq 20); -> 25Hz
(def step-sample-freq 10)
(def alc-sample-freq 10)

;; interval times in ticks: the ble-input will fire every 5th tick when there are
;; 50 ticks per second and the input sample freq is 10Hz
(def ble-input-interval (quot single-tick-freq ble-sample-freq))
(def ble-decision-interval (quot single-tick-freq ble-sample-freq))
(def accel-input-interval (quot single-tick-freq accel-sample-freq))
(def accel-decision-interval (* (quot single-tick-freq accel-sample-freq)
                                accel-sample-freq
                                accel-window-overlap))
(def alc-input-interval (quot single-tick-freq alc-sample-freq))
(def alc-decision-interval (quot single-tick-freq step-sample-freq))
(def step-input-interval (quot single-tick-freq ble-sample-freq))
(def step-decision-interval (quot single-tick-freq step-sample-freq))
