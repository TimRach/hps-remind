;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns remind.io
  [:require
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]
   [remind.state :as state]
   [remind.config :as cfg]
   [remind.entities :as ents]])

(defn read-log-data [filename]
  (when filename
    (with-open [in-file (io/reader filename)]
      (doall
       (csv/read-csv in-file :separator \,)))))

(defn find-file
  "Returns the first file matching the regex in a list of files"
  [regex files]
  (first (filter #(re-find regex (clojure.string/lower-case (.getPath %)))
                 (remove #(.isDirectory %) files))))

(defn matchNames
  "Returns a list of files in the given directory matching hardcoded regexes"
  [directory]
  (let [matchednames [#".*accel.csv$" #".*ble.csv$" #".*alcohol.csv$" #".*steps.csv$" #".*model$" #".*.confusion"]
        files (.listFiles directory)]
    (map #(find-file % files) matchednames)))

(defn load-log-data
  "Load inputfiles from the specified directory"
  [directoryname]
  (let [[acceldata bledata alcdata stepdata modeldata modelconf] (matchNames directoryname)]
    (state/reset-data (rest (read-log-data bledata)) (rest (read-log-data acceldata))
                      (rest (read-log-data alcdata)) (rest (read-log-data stepdata)))
    (reset! state/alcInput (ents/create-csvinput alcdata cfg/alc-input-interval 20))

    (reset! state/bluetoothLE (ents/create-csvinput bledata cfg/ble-input-interval
                                                    (* cfg/ble-window-size cfg/ble-sample-freq)))
    (reset! state/acceleration (ents/create-csvinput acceldata cfg/accel-input-interval
                                                     (* cfg/accel-window-size cfg/accel-sample-freq)))
    (reset! state/stepCSV (ents/create-csvinput stepdata cfg/step-input-interval 10))
    (when (and modeldata modelconf)
      (reset! state/accelerationModel
              (.readObject (java.io.ObjectInputStream. (java.io.FileInputStream. modeldata))))
      (reset! state/modelconfusion (mapv #(mapv read-string %) (read-log-data modelconf))))
    (remind.experts/setup-experts)))
