(ns remind.memory
  (:require [amalloy.ring-buffer :as rb]
            [remind.config :as cfg]))


(def zone-ring (atom (rb/ring-buffer cfg/dictator-buffer-size)))
(def move-ring (atom (rb/ring-buffer 2)))


(def state
  {:current-zone (atom nil)
   :previous-zone (atom nil)
   :entered-zone (atom :None)
   :current-move (atom nil)
   :previous-move (atom nil)})

(defn new-zone-decision [dec]
  (reset! (:previous-zone state) @(:current-zone state))
  (reset! (:current-zone state) dec))

(defn add-zone [dec]
  (swap! zone-ring conj dec))


(defn reset-state []
  (swap! zone-ring empty)
  (swap! move-ring empty)
  (reset! (:current-zone state)  nil)
  (reset! (:previous-zone state) nil)
  (reset! (:entered-zone state) :None)
  (reset! (:current-move state)  nil)
  (reset! (:previous-move state) nil)
  )
