(ns remind.entities
  (:require
   [clojure.data.csv :as csv]
   [clojure.java.io :as io]
   [amalloy.ring-buffer :as rb]
   [remind.config :as cfg]))

(defn convert-entry [entry]
  (when-not (or (nil? entry) (= "" entry))
    (read-string entry)))

(defn set-interval [callback ms] 
  (future (while true (do (Thread/sleep ms) (callback)))))

(defprotocol ClockedBehaviour
  (reset [this])
  (start [this])
  (stop [this])
  (ontick [this n] "What to do on tick n")
  (nexttick [this]))



(defrecord CSVInput [filename fields data pos frequency buffer runner buffersize timedmode]
  ClockedBehaviour
  (reset [this]
    (stop this)
    (reset! pos 0)
    (swap! buffer empty))
  (ontick [this n]
    (when (or timedmode (zero? (rem n frequency)))
      (swap! buffer conj (zipmap fields (mapv convert-entry (get data (quot n frequency)))))))
  (nexttick [this]
    (swap! pos inc)
    (ontick this @pos))
  (start [this]
    (println "reading input " (.getName filename) " each " (* frequency cfg/single-tick-time))
    (reset! timedmode true)
    (let [job (set-interval #(nexttick this) (* frequency cfg/single-tick-time))]
      (reset! runner job)))
  (stop [this]
    (when-not (nil? @runner)
      (reset! timedmode false)
      (future-cancel @runner)
      (reset! runner nil))))


(defn create-csvinput
  [filename frequency buffersize]
  (let [file (io/as-file filename)]
    (cond
      (not (.exists file))(throw (java.lang.Exception. (str "Input " filename " does not exist.")))
      ;; (< frequency 1) (throw (java.lang.Exception. "Frequency can not be less than 1."))
      (< buffersize 1) (throw (java.lang.Exception. "Buffer size can not be smaller than 1."))
      :else (with-open [reader (io/reader file)]
              (let [data (doall (csv/read-csv reader :separator \,))
                    fields (first data)]
                (CSVInput. filename (map keyword (first data)) (vec (rest data)) (atom -1)
                           (max 1 frequency) (atom (rb/ring-buffer buffersize)) (atom nil) buffersize
                           (atom false)))))))

(defrecord Clock [subscribers]
  ClockedBehaviour
  (ontick [this n]
    (println "Clock: " n)
    (doseq [sub @subscribers]
      (ontick sub n))))

