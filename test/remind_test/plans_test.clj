(ns remind-test.plans-test
  (:require
   [clojure.test :refer :all]
   [hps.utilities.math :as mut]
   [remind.plans :as plans])
  (:import [hps.types DecisionAlternative]))


(def oldmov (DecisionAlternative. :MOVING [] [] [] 0.85))
(def currmov (DecisionAlternative. :MOVING [] [] [] 0.8))
(def currzone (DecisionAlternative. :BLUE [] [] [] 0.8))
(def oldzone (DecisionAlternative. :RED [] [] [] 0.8))

(defn eval-actions [actions state]
  (zipmap (map #(:name %) actions)
          (map #(plans/eval-action % state) actions)))


(deftest operators
  (let [none (DecisionAlternative. :NONE [] [] [] 0.65)
        blue (DecisionAlternative. :BLUE [] [] [] 0.85)
        mov (DecisionAlternative. :MOVING [] [] [] 0.9)
        stand (DecisionAlternative. :STANDING [] [] [] 0.85)]
    (is (= (plans/p!= none :NONE) 0.35)
        (= (plans/p= none :NONE) 0.65))))

(deftest zone-actions
  (let [none (DecisionAlternative. :NONE [] [] [] 0.65)
        blue (DecisionAlternative. :BLUE [] [] [] 0.85)
        mov (DecisionAlternative. :MOVING [] [] [] 0.9)
        stand (DecisionAlternative. :STANDING [] [] [] 0.85)]
    (is (= (eval-actions plans/actions {:oldmov mov :currmov mov
                                        :oldzone none :currzone blue})
           {:enterzone 0.85 :exitzone 0.675 :stayinzone 0.325}))
    (is (= (eval-actions plans/actions {:oldmov stand :currmov mov
                                        :oldzone none :currzone blue})
           {:enterzone 0.85 :exitzone 0.675 :stayinzone 0.325}))
    (is (= (eval-actions plans/actions {:oldmov stand :currmov stand
                                        :oldzone blue :currzone none})
           {:enterzone 0.35 :exitzone 0.425 :stayinzone 0.575}))
    ))
