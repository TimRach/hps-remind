(ns remind-test.entities
  (:require [clojure.test :refer :all]
            [remind.entities :refer :all]
            [remind.experts :as exp]))

(deftest input-creation
  (is (thrown? Exception (create-csvinput "baba.csv" 10 10)))
  (is (instance? remind.entities.CSVInput (create-csvinput "test/remind_test/test.csv" 10 10)))
  (is (thrown? Exception (create-csvinput "test/remind_test/test.csv" 0 10)))
  (is (thrown? Exception (create-csvinput "test/remind_test/test.csv" 10 0))))


(deftest data-retrieval
  (let [csvin (create-csvinput "test/remind_test/test.csv" 1 2)
        csvin2 (create-csvinput "test/remind_test/test.csv" 2 1)]
    (is (= (:fields csvin) [:Index :COL1 :COL2 :COL3]))
    (ontick csvin 0)
    (ontick csvin2 0)
    (is (= (last @(:buffer csvin)) {:Index 0 :COL1 nil :COL2 nil :COL3 nil}))
    (is (= (last @(:buffer csvin2)) {:Index 0 :COL1 nil :COL2 nil :COL3 nil}))
    (ontick csvin 3)
    (is (= (last @(:buffer csvin)) {:Index 3 :COL1 nil :COL2 nil :COL3 -99}))
    (ontick csvin2 2)
    (is (= (last @(:buffer csvin2)) {:Index 1 :COL1 nil :COL2 nil :COL3 nil}))
    (ontick csvin 81)
    (ontick csvin2 81)
    (is (= (last @(:buffer csvin)) {:Index 81 :COL1 -93 :COL2 -94 :COL3 -88}))
    (is (= (last @(:buffer csvin2)) {:Index 1 :COL1 nil :COL2 nil :COL3 nil}))
    (is (= (count @(:buffer csvin)) 2))
    (is (= (count @(:buffer csvin2)) 1))
    (is (= (map #(:COL2 %) @(:buffer csvin)) [nil -94]))
    (is (= (filter (complement nil?) (map #(:COL2 %) @(:buffer csvin))) [-94]))
    (is (= (apply (partial merge-with vector) @(:buffer csvin)) {:Index [3 81]
                                                           :COL1 [nil -93]
                                                           :COL2 [nil -94]
                                                           :COL3 [-99 -88]}))
    ))


(deftest heuristic-funs
  (let [csvin (create-csvinput "test/remind_test/test.csv" 1 2)]
    (doseq [n (range 8 17)] (ontick csvin n))
    (is (= (exp/get-window-data @(:buffer csvin)) {:COL1 [nil nil nil nil nil nil nil nil nil] :COL2 [nil nil nil nil nil nil nil nil nil] :COL3 [nil nil nil nil nil nil nil nil nil]}))
    (is (= (exp/deviationfn [:COL1 :COL2 :COL3] @(:buffer csvin) 1 ) {:COL1 -1
                                                                      :COL2 -1
                                                                      :COL3 -1
                                                                      }))
    ;; (doseq [n (range 81 100)] (ontick csvin n))
    ;; (is (= (exp/deviationfn [:COL1 :COL2 :COL3] @(:buffer csvin)) {:COL1 (/ 4 75)
    ;;                                                                :COL2 (/ -14 75)
    ;;                                                                :COL3 (/ 2 15)}))
    ))
